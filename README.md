# Atlassian Code Exercise

### Prerequisites (Required)
* Java 8 (JDK 1.8)

### Software used for testing (Not Required)
* IntelliJ IDEA CE
* Java 8 (JDK 1.8)
* Gradle
* Sprint Boot
* Postman

### Java 8 Installation for Mac OS
* [Oracle JDK 8](https://java.com/en/download/mac_download.jsp)

### Run Tests
```bash
./gradlew test
```

### Run Server/App
```bash
./gradlew bootRun
```

### Testing
* Use your favorite REST API Client
  * Postman
  * Google Chrome REST Client
  * or others...

### Current Status

#### Assumptions
* The account-contact relationship is one to many, and contacts may not necessarily be associated with an account.
Therefore, the current implementation assumes that a Contact is only related to a single Account at any given time.
* In an ideal situation, you might want to be able to re-assign contacts to accounts or assign contacts to accounts after the fact.  

#### Database
* Current implementation uses an in-memory database leveraging H2
* Future implementations should use a deployment ready relational database such as PostgreSQL or MySQL

### Current Implementation
* Contact/Account Management
  * When an Account is deleted, the Contacts associated with that Account ARE NOT deleted
  * To associate a Contact with an Account, first create the Account, then create the Contact
    * ```POST    /accounts/ : Create a new Account```
    * ```POST    /accounts/{id}/contact : Create a new Contact for an Account```
* Account REST APIs
```
GET     /accounts/ : Get All Accounts
POST    /accounts/ : Create a new Account
POST    /accounts/{id}/contact : Create a new Contact for an Account

GET     /accounts/{id} : Get Account by ID
PUT     /accounts/{id} : Update an existing Account
DELETE  /accounts/{id} : Delete Account by ID
```
* Contact REST APIs
```
GET     /contacts/ : Get All Contacts
POST    /contacts/ : Create a new Contact

GET     /contacts/{id} : Get Contact by ID
PUT     /contacts/{id} : Update an existing Contact
DELETE  /contacts/{id} : Delete Contact by ID
```

### Planned Future Features and Tasks
* Add support for Swagger UI (OpenAPI Specification)
* Contact/Account Management
  * Move Contact to a New Account
  * Assign an existing Contact to a New Account
  * Remove Contact from an Account
  * Delete Account and Remote All Child Contacts
* Add more comments
* Support failure test cases, not just successful test cases
* Add CI support for continuous builds (Pipelines)

### Notes
* Currently the database is set to 'update' every time the application is run if it exists, or create the database if it does not exist
 In order to start from a clean state, you can remove *.db files in the base directory.


# Atlassian Code Exercise Instructions

### Atlassian Code Exercise - Senior Full Stack Developer, Marketing Technology

#### What do I need to do?

The task is relatively straightforward: build a simple application using the provided business objective and technical requirements. The goal is for us to get a feel for how you approach constructing a back end service.

Much of the requirements are underspecified (just like in the real world!), so go ahead and make assumptions to inform your decisions. We're not looking for you to come as close as possible to a reference implementation -- we just want to see how you think about putting an app together.

In terms of how much time to spend, our general guidance is a maximum of 6 hours. If you're feeling crunched, it's better to provide a written description or outline of how you'd approach something than to push yourself to get things into code.

We appreciate the investment of your time and thought.

Good luck!

#### Project requirements

- **For this role, we'd prefer solutions in Java**
- Feel free to use any libraries or frameworks you're comfortable with
- Your output should live in a [Bitbucket](https://bitbucket.org) repo -- use an alternate account if it would be inappropriate to have this exercise appear on your main account
- Your documentation/readme should have instructions on how to install dependencies and run the server
- Your reviewers will be using macOS (Sierra or High Sierra)

#### Business objective

As part of a potential feature build, the Atlassian Marketplace (MPAC) team needs a service that can store and serve basic information about Marketplace vendor accounts (companies who sell apps on [marketplace.atlassian.com](https://marketplace.atlassian.com)) and the contacts (people) who are associated with those accounts. Your role in the project is to create a simple API for a yet-to-be-designed UI layer.

The application should be relatively lightweight, but it needs to be able to scale with the growth the team expects over the coming fiscal year.

Besides Java, there is no existing tech stack or architecture to conform to, so the team is leaving the early design choices and patterns up to you, though there is an expectation that eventually the service will run on AWS infrastructure.

#### Technical requirements

- Create an application that can handle web requests
- The web server should provide a REST API for the following objects and lists of objects in application/JSON
  - Contact (GET/POST/PUT)
  - Account (GET/POST/PUT)
  - Account (GET)
    - Contacts
- The objects should be stored in a database

Contacts should have the following attributes:

- Name
- Email address
- Address line 1
- Address line 2
- City
- State
- Postal code
- Country

Accounts should have the following attributes:

- Company name
- Address line 1
- Address line 2
- City
- State
- Postal code
- Country

The account-contact relationship is one to many, and contacts may not necessarily be associated with an account.

#### Suggested milestones

- Can the server be started using the documentation?
- Does the application connect to a database?
- Does the account endpoint handle GET/POST/PUT requests?
- Does the contact endpoint handle GET/POST/PUT requests?
- Can we associate a contact with an account?
- Can we get all contacts for an account?
- Have test cases been written?

#### Delivery

As mentioned above, the best way to deliver is to create a public [Bitbucket](https://bitbucket.org) repository and send the URL to your recruiter -- they'll forward it along to the hiring manager and team for review.