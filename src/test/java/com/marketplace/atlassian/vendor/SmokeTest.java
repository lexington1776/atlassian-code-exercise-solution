package com.marketplace.atlassian.vendor;

import com.marketplace.atlassian.vendor.controller.AccountController;
import com.marketplace.atlassian.vendor.controller.ContactController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * SmokeTest
 */
@SpringBootTest
public class SmokeTest {

    @Autowired
    private ContactController contactController;

    @Autowired
    private AccountController accountController;

    @Test
    public void contextLoads() throws Exception {
        assertThat(contactController).isNotNull();
        assertThat(accountController).isNotNull();

    }
}