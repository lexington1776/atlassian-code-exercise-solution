package com.marketplace.atlassian.vendor.utils;

import com.marketplace.atlassian.vendor.model.Account;
import com.marketplace.atlassian.vendor.model.Address;
import com.marketplace.atlassian.vendor.model.Contact;

import java.sql.Timestamp;

/**
 * DataBuilder
 * useful util to build contacts and accounts for testing
 */
public class DataBuilder {
    /**
     * getContactOne
     * @return Contact
     */
    public static Contact getContactOne() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        String randomEmailAddress = timestamp.toString() + "@gmail.com";
        Address address = new Address();
        address.setAddressLine1("101 Anywhere Street");
        address.setAddressLine2("Apt 1");
        address.setCity("Anywhere");
        address.setState("TX");
        address.setPostalCode("12354");
        address.setCountry("US");

        Contact contact = new Contact();
        contact.setFirstName("John");
        contact.setLastName("Doe");
        contact.setEmailAddress(randomEmailAddress);
        contact.setAddress(address);
        return contact;
    }

    /**
     * getAccountOne
     * @return Account
     */
    public static Account getAccountOne() {
        Address address = new Address();
        address.setAddressLine1("201 Anywhere Street");
        address.setAddressLine2("Bldg 104");
        address.setCity("Anywhere");
        address.setState("TX");
        address.setPostalCode("12354");
        address.setCountry("US");

        Account account = new Account();
        account.setCompanyName("Apple");
        account.setAddress(address);
        return account;
    }
}
