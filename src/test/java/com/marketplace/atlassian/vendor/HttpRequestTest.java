package com.marketplace.atlassian.vendor;

import com.marketplace.atlassian.vendor.model.Account;
import com.marketplace.atlassian.vendor.model.Contact;
import com.marketplace.atlassian.vendor.utils.DataBuilder;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * HttpRequestTest
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpRequestTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * testContactMethods
     *
     * @throws Exception
     */
    @Test
    public void testContactMethods() throws Exception {
        String url = getBaseUrl("contacts");

        // Get All Contacts
        List<Contact> contactList = this.restTemplate.getForObject(url, List.class);
        int origListSize = contactList.size();

        // Create Contact
        Contact contact = DataBuilder.getContactOne();
        Contact createdContact = createContact(contact);
        System.out.println(createdContact);
        assertThat(createdContact.getId() != null);
        assertThat(contact.getFirstName().equals(createdContact.getFirstName()));
        assertThat(contact.getLastName().equals(createdContact.getLastName()));
        assertThat(contact.getAddress().getAddressLine1().equals(createdContact.getAddress().getAddressLine1()));

        // Get All Contacts - to test created Contact
        contactList = this.restTemplate.getForObject(url, List.class);
        assertEquals(contactList.size(), origListSize + 1);

        // Get Contact by Id
        Contact getContact = this.restTemplate.getForObject(url + createdContact.getId(), Contact.class);
        assertThat(contact.getFirstName().equals(getContact.getFirstName()));
        assertThat(contact.getLastName().equals(getContact.getLastName()));
        assertThat(contact.getAddress().getAddressLine1().equals(getContact.getAddress().getAddressLine1()));


        // Update Contact by Id
        getContact.setFirstName("Jason");
        this.restTemplate.put(url + createdContact.getId(), getContact);
        // Get Updated Contact by Id
        Contact updatedContact = this.restTemplate.getForObject(url + createdContact.getId(), Contact.class);
        assertThat(updatedContact.getFirstName().equals("Jason"));

        // Delete Contact
        this.restTemplate.delete(url + createdContact.getId());

        // Get All Contacts - to test deleted Contact
        contactList = this.restTemplate.getForObject(url, List.class);
        assertEquals(contactList.size(), origListSize);

    }

    /**
     * testAccountMethods
     *
     * @throws Exception
     */
    @Test
    public void testAccountMethods() throws Exception {
        String url = "http://localhost:" + port + "/accounts/";

        // Get All Accounts
        List<Account> accountList = this.restTemplate.getForObject(url, List.class);
        int origListSize = accountList.size();
        System.out.println(origListSize);

        // Create Account
        Account account = DataBuilder.getAccountOne();
        Account createdAccount = this.restTemplate.postForObject(url, account, Account.class);
        assertThat(createdAccount.getId() != null);
        assertThat(account.getCompanyName().equals(createdAccount.getCompanyName()));
        assertThat(account.getAddress().getAddressLine1().equals(createdAccount.getAddress().getAddressLine1()));

        // Create Contact and Assign Account
        Contact contact = DataBuilder.getContactOne();
        Account accountWithContact = createContactForAccount(contact, createdAccount);
        assertThat(accountWithContact.getId() != null);
        assertThat(accountWithContact.getContacts().size() > 0);

        // Get All Accounts - to test created Account + Contact
        accountList = this.restTemplate.getForObject(url, List.class);
        assertEquals(accountList.size(), origListSize + 1);

        // Get Account by Id
        Account getAccount = this.restTemplate.getForObject(url + accountWithContact.getId(), Account.class);
        assertThat(accountWithContact.getCompanyName().equals(getAccount.getCompanyName()));
        assertThat(account.getAddress().getAddressLine1().equals(getAccount.getAddress().getAddressLine1()));
        assertThat(getAccount.getContacts().size() > 0);

        // Update Account by Id
        getAccount.setCompanyName("Alphabet");
        this.restTemplate.put(url + getAccount.getId(), getAccount);
        // Get Updated Account by Id
        Account updatedAccount = this.restTemplate.getForObject(url + getAccount.getId(), Account.class);
        assertThat(updatedAccount.getCompanyName().equals("Alphabet"));

        // Delete Account
        this.restTemplate.delete(url + createdAccount.getId());

        // Get All Accounts - to test deleted Account
        accountList = this.restTemplate.getForObject(url, List.class);
        assertEquals(accountList.size(), origListSize);

    }

    private String getBaseUrl(String type) {
        return "http://localhost:" + port + "/" + type + "/";
    }

    /**
     * createContact
     * POST    /contacts/ : Create a new Contact
     *
     * @param contact
     * @return Created Contact
     */
    private Contact createContact(Contact contact) {
        String url = getBaseUrl("contacts");
        return this.restTemplate.postForObject(url, contact, Contact.class);
    }

    /**
     * createContactForAccount
     * POST    /accounts/{id}/contact : Create a new Contact for an Account
     *
     * @param contact
     * @param account
     * @return
     */
    private Account createContactForAccount(Contact contact, Account account) {
        String url = getBaseUrl("accounts") + String.valueOf(account.getId()) + "/contact/";
        return this.restTemplate.postForObject(url, contact, Account.class);
    }

}
