package com.marketplace.atlassian.vendor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketplace.atlassian.vendor.model.Contact;
import com.marketplace.atlassian.vendor.service.ContactService;
import com.marketplace.atlassian.vendor.utils.DataBuilder;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * ContactWebApplicationTest
 */
@SpringBootTest
@AutoConfigureMockMvc
public class ContactWebApplicationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ContactService contactService;

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * getAllContacts
     *
     * @throws Exception
     */
    @Test
    public void getAllContacts() throws Exception {
        List<Contact> allContacts = Arrays.asList(DataBuilder.getContactOne());

        BDDMockito.given(contactService.getAllContacts()).willReturn(allContacts);

        mvc.perform(MockMvcRequestBuilders.get("/contacts/")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(allContacts.size())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].address.addressLine1").isNotEmpty());
    }

    /**
     * getContactById
     *
     * @throws Exception
     */
    @Test
    public void getContactById() throws Exception {
        Contact contact = DataBuilder.getContactOne();

        BDDMockito.given(contactService.getContactById(1L)).willReturn(contact);

        mvc.perform(MockMvcRequestBuilders.get("/contacts/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(contact.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address.addressLine1").value(contact.getAddress().getAddressLine1()));
    }

    /**
     * createContact
     *
     * @throws Exception
     */
    @Test
    public void createContact() throws Exception {
        Contact contact = DataBuilder.getContactOne();
        BDDMockito.when(contactService.createContact(any(Contact.class))).thenReturn(contact);

        mvc.perform(MockMvcRequestBuilders
                .post("/contacts/")
                .content(asJsonString(contact))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(contact.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address.addressLine1").value(contact.getAddress().getAddressLine1()));
    }

    /**
     * updateContact
     *
     * @throws Exception
     */
    @Test
    public void updateContact() throws Exception {
        Contact contact = DataBuilder.getContactOne();
        contact.setFirstName("June");
        BDDMockito.when(contactService.updateContact(any(Contact.class), any(Long.class))).thenReturn(contact);

        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders
                .put("/contacts/{id}", 1L)
                .content(asJsonString(contact))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(contact.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address.addressLine1").value(contact.getAddress().getAddressLine1()));
    }

    /**
     * deleteContact
     *
     * @throws Exception
     */
    @Test
    public void deleteContact() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/contacts/{id}", 1L))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

}

