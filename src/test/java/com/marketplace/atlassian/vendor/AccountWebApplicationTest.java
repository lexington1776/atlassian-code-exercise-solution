package com.marketplace.atlassian.vendor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketplace.atlassian.vendor.model.Account;
import com.marketplace.atlassian.vendor.service.AccountService;
import com.marketplace.atlassian.vendor.utils.DataBuilder;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * AccountWebApplicationTest
 */
@SpringBootTest
@AutoConfigureMockMvc
public class AccountWebApplicationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountService accountService;

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * getAllAccounts
     *
     * @throws Exception
     */
    @Test
    public void getAllAccounts() throws Exception {
        List<Account> allAccounts = Arrays.asList(DataBuilder.getAccountOne());

        BDDMockito.given(accountService.getAllAccounts()).willReturn(allAccounts);

        mvc.perform(MockMvcRequestBuilders.get("/accounts/")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(allAccounts.size())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].companyName").isNotEmpty());
    }

    /**
     * getAccountById
     *
     * @throws Exception
     */
    @Test
    public void getAccountById() throws Exception {
        Account account = DataBuilder.getAccountOne();

        BDDMockito.given(accountService.getAccountById(1L)).willReturn(account);

        mvc.perform(MockMvcRequestBuilders.get("/accounts/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.companyName").value(account.getCompanyName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address.addressLine1").value(account.getAddress().getAddressLine1()));
    }

    /**
     * createAccount
     *
     * @throws Exception
     */
    @Test
    public void createAccount() throws Exception {
        Account account = DataBuilder.getAccountOne();
        BDDMockito.when(accountService.createAccount(any(Account.class))).thenReturn(account);

        mvc.perform(MockMvcRequestBuilders
                .post("/accounts/")
                .content(asJsonString(account))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.companyName").value(account.getCompanyName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address.addressLine1").value(account.getAddress().getAddressLine1()));
    }

    /**
     * updateAccount
     *
     * @throws Exception
     */
    @Test
    public void updateAccount() throws Exception {
        Account account = DataBuilder.getAccountOne();
        account.setCompanyName("Lowes");
        BDDMockito.when(accountService.updateAccount(any(Account.class), any(Long.class))).thenReturn(account);

        mvc.perform(MockMvcRequestBuilders
                .put("/accounts/{id}", 1L)
                .content(asJsonString(account))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.companyName").value(account.getCompanyName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address.addressLine1").value(account.getAddress().getAddressLine1()));
    }

    /**
     * deleteAccount
     *
     * @throws Exception
     */
    @Test
    public void deleteAccount() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/accounts/{id}", 1L))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

}

