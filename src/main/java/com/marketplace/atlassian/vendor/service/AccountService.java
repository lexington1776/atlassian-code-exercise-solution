package com.marketplace.atlassian.vendor.service;

import com.marketplace.atlassian.vendor.exception.RecordNotFoundException;
import com.marketplace.atlassian.vendor.model.Account;
import com.marketplace.atlassian.vendor.model.Contact;
import com.marketplace.atlassian.vendor.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * AccountService
 */
@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    /**
     * getAllAccounts
     *
     * @return List of Accounts
     */
    public List<Account> getAllAccounts() {
        List<Account> accountList = accountRepository.findAll();

        if (accountList.size() > 0) {
            return accountList;
        } else {
            return new ArrayList<Account>();
        }
    }

    /**
     * getAccountById
     *
     * @param id
     * @return Account
     * @throws RecordNotFoundException
     */
    public Account getAccountById(Long id) throws RecordNotFoundException {
        Optional<Account> account = accountRepository.findById(id);
        if (account.isPresent()) {
            return account.get();
        } else {
            throw new RecordNotFoundException("No account record exist for given id");
        }
    }

    /**
     * createAccount
     *
     * @param account
     * @return Created Account
     */
    public Account createAccount(Account account) {
        return accountRepository.save(account);
    }

    /**
     * addContact (to Account)
     *
     * @param accountId
     * @param contact
     * @return Account with Contact
     * @throws RecordNotFoundException
     */
    public Account addContact(long accountId, Contact contact) throws RecordNotFoundException {
        Optional<Account> account = accountRepository.findById(accountId);

        if (account.isPresent()) {
            Account newAccount = account.get();
            newAccount.addContact(contact);
            newAccount = accountRepository.save(newAccount);

            return newAccount;
        } else {
            throw new RecordNotFoundException("No contact record exist for given id");
        }
    }

    /**
     * updateAccount
     *
     * @param entity
     * @param id
     * @return Updated Account
     * @throws RecordNotFoundException
     */
    public Account updateAccount(Account entity, long id) throws RecordNotFoundException {
        Optional<Account> account = accountRepository.findById(id);

        if (account.isPresent()) {
            Account newEntity = account.get();
            newEntity.setCompanyName(entity.getCompanyName());
            newEntity.setAddress(entity.getAddress());

            newEntity = accountRepository.save(newEntity);

            return newEntity;
        } else {
            throw new RecordNotFoundException("No account record exist for given id");
        }
    }

    /**
     * deleteAccountById
     *
     * @param id
     * @throws RecordNotFoundException
     */
    public void deleteAccountById(Long id) throws RecordNotFoundException {
        Optional<Account> account = accountRepository.findById(id);

        if (account.isPresent()) {
            accountRepository.deleteById(id);
        } else {
            throw new RecordNotFoundException("No account record exist for given id");
        }
    }
}