package com.marketplace.atlassian.vendor.service;

import com.marketplace.atlassian.vendor.exception.RecordNotFoundException;
import com.marketplace.atlassian.vendor.model.Contact;
import com.marketplace.atlassian.vendor.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * ContactService
 */
@Service
public class ContactService {

    @Autowired
    ContactRepository contactRepository;

    /**
     * getAllContacts
     *
     * @return List of Contacts
     */
    public List<Contact> getAllContacts() {
        List<Contact> contactList = contactRepository.findAll();

        if (contactList.size() > 0) {
            return contactList;
        } else {
            return new ArrayList<Contact>();
        }
    }

    /**
     * getContactById
     *
     * @param id
     * @return Contact
     * @throws RecordNotFoundException
     */
    public Contact getContactById(Long id) throws RecordNotFoundException {
        Optional<Contact> contact = contactRepository.findById(id);
        if (contact.isPresent()) {
            return contact.get();
        } else {
            throw new RecordNotFoundException("No contact record exist for given id");
        }
    }

    /**
     * createContact
     *
     * @param contact
     * @return Created Contact
     */
    public Contact createContact(Contact contact) {
        return contactRepository.save(contact);
    }

    /**
     * updateContact
     *
     * @param entity
     * @param id
     * @return Updated Contact
     * @throws RecordNotFoundException
     */
    public Contact updateContact(Contact entity, long id) throws RecordNotFoundException {
        Optional<Contact> contact = contactRepository.findById(id);

        if (contact.isPresent()) {
            Contact newEntity = contact.get();
            newEntity.setEmailAddress(entity.getEmailAddress());
            newEntity.setFirstName(entity.getFirstName());
            newEntity.setLastName(entity.getLastName());

            newEntity = contactRepository.save(newEntity);

            return newEntity;
        } else {
            throw new RecordNotFoundException("No contact record exist for given id");
        }
    }

    /**
     * deleteContactById
     *
     * @param id
     * @throws RecordNotFoundException
     */
    public void deleteContactById(Long id) throws RecordNotFoundException {
        Optional<Contact> contact = contactRepository.findById(id);

        if (contact.isPresent()) {
            contactRepository.deleteById(id);
        } else {
            throw new RecordNotFoundException("No contact record exist for given id");
        }
    }
}