package com.marketplace.atlassian.vendor.controller;

import com.marketplace.atlassian.vendor.exception.RecordNotFoundException;
import com.marketplace.atlassian.vendor.model.Contact;
import com.marketplace.atlassian.vendor.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Contact Controller
 * <p>
 * GET     /contacts/ : Get All Contacts
 * POST    /contacts/ : Create a new Contact
 * <p>
 * GET     /contacts/{id} : Get Contact by ID
 * PUT     /contacts/{id} : Update an existing Contact
 * DELETE  /contacts/{id} : Delete Contact by ID
 */
@RestController
@RequestMapping(path = "/contacts")
public class ContactController {
    @Autowired
    private ContactService contactService;

    /**
     * getAllContacts
     * @return List of Contacts
     */
    @GetMapping(path = "/")
    public ResponseEntity<List<Contact>> getAllContacts() {
        List<Contact> list = contactService.getAllContacts();
        return new ResponseEntity<List<Contact>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * createContact
     * @param contact
     * @return Created Contact
     */
    @PostMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Contact> createContact(@RequestBody Contact contact) {
        Contact created = contactService.createContact(contact);
        return new ResponseEntity<Contact>(created, new HttpHeaders(), HttpStatus.CREATED);
    }

    /**
     * getContactById
     * @param id
     * @return Contact
     * @throws RecordNotFoundException
     */
    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity<Contact> getContactById(@PathVariable("id") Long id)
            throws RecordNotFoundException {
        Contact entity = contactService.getContactById(id);
        return new ResponseEntity<Contact>(entity, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * updateContact
     * @param contact
     * @param id
     * @return Updated Contact
     * @throws RecordNotFoundException
     */
    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Contact> updateContact(@RequestBody Contact contact, @PathVariable("id") Long id)
            throws RecordNotFoundException {
        Contact created = contactService.updateContact(contact, id);
        return new ResponseEntity<Contact>(created, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * deleteContactById
     * @param id
     * @return HttpStatus
     * @throws RecordNotFoundException
     */
    @DeleteMapping(path = "/{id}")
    public HttpStatus deleteContactById(@PathVariable("id") Long id)
            throws RecordNotFoundException {
        contactService.deleteContactById(id);
        return HttpStatus.OK;
    }

}
