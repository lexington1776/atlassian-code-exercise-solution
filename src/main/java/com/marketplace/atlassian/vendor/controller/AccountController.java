package com.marketplace.atlassian.vendor.controller;

import com.marketplace.atlassian.vendor.exception.RecordNotFoundException;
import com.marketplace.atlassian.vendor.model.Account;
import com.marketplace.atlassian.vendor.model.Contact;
import com.marketplace.atlassian.vendor.service.AccountService;
import com.marketplace.atlassian.vendor.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * AccountController
 * <p>
 * REST Descriptions
 * GET     /accounts/ : Get All Accounts
 * POST    /accounts/ : Create a new Account
 * POST    /accounts/{id}/contact : Create a new Contact for an Account
 * <p>
 * GET     /accounts/{id} : Get Account by ID
 * PUT     /accounts/{id} : Update an existing Account
 * DELETE  /accounts/{id} : Delete Account by ID
 */
@RestController
@RequestMapping(path = "/accounts")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private ContactService contactService;

    /**
     * getAllAccounts
     *
     * @return List of Accounts
     */
    @GetMapping(path = "/")
    public ResponseEntity<List<Account>> getAllAccounts() {
        List<Account> list = accountService.getAllAccounts();
        return new ResponseEntity<List<Account>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * createAccount
     *
     * @param account
     * @return Created Account
     */
    @PostMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Account> createAccount(@RequestBody Account account) {
        Account created = accountService.createAccount(account);
        return new ResponseEntity<Account>(created, new HttpHeaders(), HttpStatus.CREATED);
    }

    /**
     * createContactForAccount
     *
     * @param id
     * @param contact
     * @return Account with newly created Contact
     * @throws RecordNotFoundException
     */
    @PostMapping(path = "/{id}/contact", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Account> createContactForAccount(@PathVariable("id") Long id, @RequestBody Contact contact)
            throws RecordNotFoundException {
        Contact createdContact = contactService.createContact(contact);
        Account account = accountService.addContact(id, createdContact);
        return new ResponseEntity<Account>(account, new HttpHeaders(), HttpStatus.CREATED);
    }

    /**
     * getAccountById
     *
     * @param id
     * @return Account
     * @throws RecordNotFoundException
     */
    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity<Account> getAccountById(@PathVariable("id") Long id)
            throws RecordNotFoundException {
        Account entity = accountService.getAccountById(id);
        return new ResponseEntity<Account>(entity, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * updateAccount
     *
     * @param account
     * @param id
     * @return Updated Account
     * @throws RecordNotFoundException
     */
    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Account> updateAccount(@RequestBody Account account, @PathVariable("id") Long id)
            throws RecordNotFoundException {
        Account created = accountService.updateAccount(account, id);
        return new ResponseEntity<Account>(created, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * deleteAccountById
     *
     * @param id
     * @return HTTPStatus
     * @throws RecordNotFoundException
     */
    @DeleteMapping(path = "/{id}")
    public HttpStatus deleteAccountById(@PathVariable("id") Long id)
            throws RecordNotFoundException {
        accountService.deleteAccountById(id);
        return HttpStatus.OK;
    }

}
