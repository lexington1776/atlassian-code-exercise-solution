package com.marketplace.atlassian.vendor.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Account
 */
@Entity(name = "Account")
@Table(name = "Account", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id"),
        @UniqueConstraint(columnNames = "company_name")})
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "company_name", nullable = false, length = 200)
    private String companyName;

    @Embedded
    private Address address;

    @OneToMany(
            cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE},
            fetch = FetchType.EAGER
    )
    private List<Contact> contacts = new ArrayList<>();

    /**
     * Default Constructor
     */
    public Account() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", companyName='" + companyName + '\'' +
                ", address=" + address +
                ", contacts=" + contacts +
                '}';
    }

}
