package com.marketplace.atlassian.vendor.repository;

import com.marketplace.atlassian.vendor.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for Accounts
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
}
