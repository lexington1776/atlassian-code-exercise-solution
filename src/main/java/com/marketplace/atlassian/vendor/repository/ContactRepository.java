package com.marketplace.atlassian.vendor.repository;

import com.marketplace.atlassian.vendor.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for Contacts
 */
@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
}
